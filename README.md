
# COMP3211 Coursework 2

Both services and the client run using Python and Flask.

There are a few dependencies which can be installed using `pip install`:
```
flask
flask_restful
flask_sqlalchemy
```

To run the two web services, execute the following commands in separate terminal windows
```
python Places app\app.py
python Quotes app\app.py
```

To run the client:
`python ClientHtml\app.py`

Finally, to access the client, visit `localhost:80/`

If you wish to delete the existing databases and start from scratch, new databases can be created and populated by visiting:
```
127.0.0.1:5000/init for the Places API
127.0.0.1:5001/init for the Quotes API
```