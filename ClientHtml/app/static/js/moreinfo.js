//when the client loads, retrieve available places
const placeID = window.location.href.split("id=")[1];
let placeInfo;
document.addEventListener("DOMContentLoaded", async () => {
	//store the element that will receive the places
	let placesListElement = document.getElementById("place");
	placeInfo = await getPlaceFullInfo(placeID);
	if (!placeInfo) {
		document.getElementById('quote-form').innerHTML = "Place not Found";
		document.getElementById('place-loading').style.display = "none";
		return;
	}
	weather = await getPlaceWeather(placeInfo.name);
	createPlaceItem(placeID, placeInfo, weather, placesListElement);

	document.getElementById('place-loading').style.display = "none";
	document.getElementById('get-quote-btn').addEventListener("click", async () => {
		const request = await fetch("http://127.0.0.1:5001/quote/" + placeInfo.name + "/" + document.getElementById("numberofpeople").value);
		const data = await request.json();
		document.getElementById('quote-price').innerHTML = "the price is " + data.quote;
	});
});

function createPlaceItem(id, place, weather, list) {
	//create item and set id
	let placeDiv = document.createElement("div");
	placeDiv.id = "place-" + id;
	placeDiv.classList.add('place-item');

	//create item image
	var img = document.createElement('img');
	img.src = 'http://127.0.0.1:5000/image/' + id;
	placeDiv.appendChild(img);

	//create item bottom part
	let bottomDiv = document.createElement("div");
	bottomDiv.classList.add('item-bottom');

	let bottomDivLeft = document.createElement("div");
	bottomDivLeft.classList.add('item-bottom-side');
	let bottomDivRight = document.createElement("div");
	bottomDivRight.classList.add('item-bottom-side');
	bottomDiv.appendChild(bottomDivLeft);
	bottomDiv.appendChild(bottomDivRight);

	let bottomDivLeftTop = document.createElement("div");
	bottomDivLeftTop.innerHTML = place.name;
	let bottomDivLeftBottom = document.createElement("div");
	let moreInfoDesc = document.createElement("div");
	moreInfoDesc.classList.add("desc");
	moreInfoDesc.innerHTML = place.description;
	bottomDivLeftBottom.appendChild(moreInfoDesc);
	bottomDivLeft.appendChild(bottomDivLeftTop);
	bottomDivLeft.appendChild(bottomDivLeftBottom);

	if (placeInfo.status != 404) {
		let bottomDivRightTop = document.createElement("div");
		let weatherSimbol = document.createElement("div");
		weatherSimbol.classList.add('weather-icon');
		weatherSimbol.style.backgroundImage = "url(" + weather.current.condition.icon + ")";
		bottomDivRightTop.appendChild(weatherSimbol);
		let bottomDivRightBottom = document.createElement("div");
		bottomDivRightBottom.innerHTML = weather.current.temp_c + "°";
		bottomDivRight.appendChild(bottomDivRightTop);
		bottomDivRight.appendChild(bottomDivRightBottom);
	}


	placeDiv.appendChild(bottomDiv);
	//add class and add item to list
	placeDiv.classList.add('place-item');
	list.appendChild(placeDiv);
}

//get information about the place with id
async function getPlaceFullInfo(id) {
	const request = await fetch("http://127.0.0.1:5000/placefull/" + id);
	if (request.status == 404) {
		return null;
	}
	const data = await request.json();
	return data;
}
//get the weather of the place with city name
async function getPlaceWeather(city) {
	const request = await fetch("http://api.weatherapi.com/v1/current.json?key=a27e27d242fe4d0280d04955211111&q=" + city);
	const data = await request.json();
	return data;
}
