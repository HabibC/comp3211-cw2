//when the client loads, retrieve available places
document.addEventListener("DOMContentLoaded", async () => {
	//store the element that will receive the places
	let placesListElement = document.getElementById("places-list");
	//request the list of places
	const placesArray = await getAvailablePlaces();
	//for every available place get info and weather info and add it to the list
	for (let i = 0; i < placesArray.places.length; i++) {
		const placeInfo = placesArray.places[i];
		const weather = await getPlaceWeather(placeInfo.name);
		createPlaceItem(placesArray.places[i].id, placeInfo, weather, placesListElement);
	}
	document.getElementById('places-loading').style.display = "none";
});

function createPlaceItem(id, place, weather, list) {
	//create item and set id
	let placeDiv = document.createElement("div");
	placeDiv.id = "place-" + id;
	placeDiv.classList.add('place-item');

	//create item image
	var img = document.createElement('img');
	img.src = 'http://127.0.0.1:5000/image/' + id;
	placeDiv.appendChild(img);

	//create item bottom part
	let bottomDiv = document.createElement("div");
	bottomDiv.classList.add('item-bottom');

	let bottomDivLeft = document.createElement("div");
	bottomDivLeft.classList.add('item-bottom-side');
	let bottomDivRight = document.createElement("div");
	bottomDivRight.classList.add('item-bottom-side');
	bottomDiv.appendChild(bottomDivLeft);
	bottomDiv.appendChild(bottomDivRight);

	let bottomDivLeftTop = document.createElement("div");
	bottomDivLeftTop.innerHTML = place.name;
	let bottomDivLeftBottom = document.createElement("div");
	let moreInfoButton = document.createElement("button");
	moreInfoButton.classList.add('moreinfo-btn');
	moreInfoButton.innerHTML = "more info";
	moreInfoButton.addEventListener('click', () => { window.location.href = 'more-info?id=' + id });
	bottomDivLeftBottom.appendChild(moreInfoButton);
	bottomDivLeft.appendChild(bottomDivLeftTop);
	bottomDivLeft.appendChild(bottomDivLeftBottom);

	let bottomDivRightTop = document.createElement("div");
	let weatherSimbol = document.createElement("div");
	weatherSimbol.classList.add('weather-icon');
	weatherSimbol.style.backgroundImage = "url(" + weather.current.condition.icon + ")";
	bottomDivRightTop.appendChild(weatherSimbol);
	let bottomDivRightBottom = document.createElement("div");
	bottomDivRightBottom.innerHTML = weather.current.temp_c + "°";
	bottomDivRight.appendChild(bottomDivRightTop);
	bottomDivRight.appendChild(bottomDivRightBottom);


	placeDiv.appendChild(bottomDiv);
	//add class and add item to list
	placeDiv.classList.add('place-item');
	list.appendChild(placeDiv);
}

//get list of available places and add them to the list
async function getAvailablePlaces() {
	const request = await fetch("http://127.0.0.1:5000/allplaces");
	const data = await request.json();
	return data;
}
//get the weather of the place with city name
async function getPlaceWeather(city) {
	const request = await fetch("http://api.weatherapi.com/v1/current.json?key=a27e27d242fe4d0280d04955211111&q=" + city);
	const data = await request.json();
	return data;
}
