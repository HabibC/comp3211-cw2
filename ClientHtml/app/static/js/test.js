let body;
document.addEventListener("DOMContentLoaded", async () => {
	body = document.getElementById('body')
	getAvailablePlaces();
	getPlaceImageById(1);
	getPlaceFullInfo(1);
	getPlaceWeather("Dubai")
	getQuoteByPeopleAndCity(1, "Dubai");
});

async function getAvailablePlaces() {
	let time1 = performance.now();
	const request = await fetch("http://127.0.0.1:5000/allplaces");
	let time2 = performance.now();
	let newElement = document.createElement("div");
	newElement.innerHTML = 'The time to call <span style="color:#1f13dd;">Places API</span> for <span style="color:#12a912;">/allplaces</span> was <span>' + (time2 - time1) / 1000 + ' s</span>'
	body.appendChild(newElement);
}
async function getPlaceFullInfo(id) {
	let time1 = performance.now();
	const request = await fetch("http://127.0.0.1:5000/placefull/" + id);
	let time2 = performance.now();
	let newElement = document.createElement("div");
	newElement.innerHTML = 'The time to call <span style="color:#1f13dd;">Places API</span> for <span style="color:#12a912;">/placefull/&lt;place_id&gt;</span> was <span>' + (time2 - time1) / 1000 + ' s</span>'
	body.appendChild(newElement);
}
async function getPlaceImageById(id) {
	let time1 = performance.now();
	const request = await fetch("http://127.0.0.1:5000/image/" + id, { mode: 'no-cors' });
	let time2 = performance.now();
	let newElement = document.createElement("div");
	newElement.innerHTML = 'The time to call <span style="color:#1f13dd;">Places API</span> for <span style="color:#12a912;">/image/&lt;place_id&gt;</span> was <span>' + (time2 - time1) / 1000 + ' s</span>'
	body.appendChild(newElement);
}

async function getQuoteByPeopleAndCity(numpeople, city) {
	let time1 = performance.now();
	const request = await fetch("http://127.0.0.1:5001/quote/" + city + "/" + numpeople);
	let time2 = performance.now();
	let newElement = document.createElement("div");
	newElement.innerHTML = 'The time to call <span style="color:#1f13dd;">Quotes API</span> for <span style="color:#12a912;">/quote/&lt;places_name&gt;/&lt;num_people&gt;</span> was <span>' + (time2 - time1) / 1000 + ' s</span>'
	body.appendChild(newElement);
}

//get the weather of the place with city name
async function getPlaceWeather(city) {
	let time1 = performance.now();
	const request = await fetch("http://api.weatherapi.com/v1/current.json?key=a27e27d242fe4d0280d04955211111&q=" + city);
	let time2 = performance.now();
	let newElement = document.createElement("div");
	newElement.innerHTML = 'The time to call <span style="color:#1f13dd;">Weather API</span> for <span style="color:#12a912;">/v1/current.json?key=&lt;key&gt;&city=&lt;city&gt;</span> was <span>' + (time2 - time1) / 1000 + ' s</span>'
	body.appendChild(newElement);
}
