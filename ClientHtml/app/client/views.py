from flask import Flask, Blueprint, render_template

client = Blueprint('client', __name__)


@client.route('/')
def index_route():
    return render_template('index.html')


@client.route('/more-info')
def more_info_route():
    return render_template('more-info.html')


@client.route('/test')
def test_route():
    return render_template('test.html')
