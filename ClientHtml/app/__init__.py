from flask import Flask

app = Flask(__name__)

from app.client.views import client

app.register_blueprint(client)
