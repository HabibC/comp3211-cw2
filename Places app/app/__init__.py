from flask import Flask,request
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api
from os import urandom, path

app = Flask(__name__)
api = Api(app)

basedir = path.abspath(path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + path.join(basedir, "database.db")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

SECRET_KEY = urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

from app.places.views import places

app.register_blueprint(places)

from app.models import Places
db.create_all()
