from app import db


class Places(db.Model):
    __tablename__ = "places"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    info = db.Column(db.String(512), nullable=False)
    image_path = db.Column(db.String(128))

    def __init__(self, name, info, image_path,):
        self.name = name
        self.info = info
        self.image_path = image_path
