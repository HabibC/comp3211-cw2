from flask import Blueprint, send_file
from flask_restful import Resource, Api
from app import api, db
from app.models import Places


places = Blueprint('places', __name__)

@places.route("/init")
def init_places():
    london = Places("London", "Pretty pink restaurants, futuristic space-age toilets and jungle skyline views are just some of our favorite things about London. And of course, all the classics: Big Ben, red phone boxes and world-class museums and galleries", "london.jpg")
    bali = Places("Bali", "You’ll find beaches, volcanoes, Komodo dragons and jungles sheltering elephants, orangutans and tigers. Basically, it’s paradise. It’s likely you’ve seen an image of Bali on social media at least once in the past seven days, as it’s such a popular bucket list destination for 2021.", "bali.jpg")
    marrakesh = Places("Marrakech", "This ancient walled city is home to mosques, palaces and lush gardens. It’s known as The Red City thanks to the color of the brick walls surrounding the city. The medina is a UNESCO World Heritage Centre.", "marrakech.jpg")
    sydney = Places("Sydney", "Sydney is known around the world as one of the greatest and most iconic cities on the planet. Amazing things to do aren’t hard to find; the city has gorgeous beaches, great cafes and world-class entertainment on offer wherever you look.", "sydeny.jpg")
    maldivies = Places("The Maldives", "This tropical nation in the Indian Ocean is made up of more than 1,000 coral islands. It’s home to some of the world’s most luxurious hotel resorts, with white sandy beaches, underwater villas and restaurants and bright blue waters.", "maldives.jpg")
    paris = Places("Paris", "One of the most iconic cities in the world, Paris tops many people’s bucket lists. You’ll see so many famous landmarks here: the Eiffel Tower, Arc de Triomphe, Notre Dame cathedral—the list is never-ending.", "paris.jpg")
    dubai = Places("Dubai", "The high-flying city of the U.A.E, Dubai is one of the most glamorous destinations you’ll ever visit, and is particularly popular with Big 7 Travel readers.", "dubai.jpg")
    new_york = Places("New York", "New York is one of America’s most exciting states. With charming upstate scenery, world-class cuisine and culture and more things to do than you could fit in one lifetime. The city’s five boroughs all have special features; it’s almost impossible to narrow it down", "new_york.jpg")
    dubrovnik = Places("Dubrovnik", "As George Bernard Shaw once said, 'Those who seek paradise on Earth should come to Dubrovnik.' With its winding streets, cliffside beach bars and UNESCO World Heritage Site of the Old Town, it’s no wonder Dubrovnik is such a popular spot.", "dubrovnik.jpg")

    if not Places.query.all():
        db.session.add(london)
        db.session.add(bali)
        db.session.add(marrakesh)
        db.session.add(sydney)
        db.session.add(maldivies)
        db.session.add(paris)
        db.session.add(dubai)
        db.session.add(new_york)
        db.session.add(dubrovnik)

        db.session.commit()
    return {'init': 'True'}


class ListPlacesMin(Resource):
    def get(self):
        placesList = []
        places = Places.query.all()
        for place in places:
            placesList.append({"id": place.id, "name": place.name})
        return {"places": placesList}, 200, {'Access-Control-Allow-Origin': '*'}


api.add_resource(ListPlacesMin, '/allplaces')


class PlaceFull(Resource):
    def get(self, place_id):
        place = Places.query.get(place_id)
        if place is None:
            return {"name": "Place not found", "description": "This place was not found"}, 404, {'Access-Control-Allow-Origin': '*'}
        return {"name": place.name, "description": place.info}, 200, {'Access-Control-Allow-Origin': '*'}


api.add_resource(PlaceFull, '/placefull/<place_id>')


class DisplayImage(Resource):
    def get(self, place_id):
        place = Places.query.get(place_id)
        if place is None:
            return send_file('static/images/place_not_found.png', mimetype='image/jpg')
        return send_file('static/images/'+place.image_path, mimetype='image/'+place.image_path.split(".")[1])


api.add_resource(DisplayImage, '/image/<place_id>')
