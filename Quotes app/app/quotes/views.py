from flask import Flask, Blueprint, jsonify
from flask_restful import Resource, Api
from app import api, db
from app.models import Quotes

quotes = Blueprint('quotes', __name__)


@quotes.route('/init')
# Adds data if table is empty
def init_quotes():
    if not Quotes.query.all():
        db.session.add(Quotes("London", 129))
        db.session.add(Quotes("Bali", 823))
        db.session.add(Quotes("Marrakech", 110))
        db.session.add(Quotes("Sydney", 1203))
        db.session.add(Quotes("The Maldives", 1537))
        db.session.add(Quotes("Paris", 179))
        db.session.add(Quotes("Dubai", 652))
        db.session.add(Quotes("New York", 555))
        db.session.add(Quotes("Dubrovnik", 350))

        db.session.commit()

    return {'init': 'True'}


class GetQuote(Resource):
    def get(self, places_name, num_people):
        if int(num_people) > 10:
            return {'Error': 'Maximum 10 people'}
        request = Quotes.query.filter(
            Quotes.places_name.like(places_name)).first()
        if not request:
            return {'Error' : 'No quote available'}
        price_pp = request.price_pp
        quote = float(price_pp) * float(num_people)
        return {'place': places_name, 'num_people': num_people, 'quote': quote}, 200, {'Access-Control-Allow-Origin': '*'}


api.add_resource(GetQuote, '/quote/<places_name>/<num_people>')
