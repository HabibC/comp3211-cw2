from app import db


class Quotes(db.Model):
    __tablename__ = "quotes"

    id = db.Column(db.Integer, primary_key=True)
    places_name = db.Column(db.String(64), nullable=False)
    # Holds the price per person in pounds
    price_pp = db.Column(db.Float)

    def __init__(self, places_name, price_pp):
        self.places_name = places_name
        self.price_pp = price_pp
