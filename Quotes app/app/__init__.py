from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api
from os import urandom, path

from sqlalchemy.orm.query import Query

app = Flask(__name__)
api = Api(app)

basedir = path.abspath(path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + path.join(basedir, "database.db")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

SECRET_KEY = urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

from app.quotes.views import quotes


app.register_blueprint(quotes)

from app.models import Quotes
db.create_all()